import React from 'react';

export default function Task({ task, toggleTask }){

    function handleToggleTask(){
        toggleTask(task.uid)
    }

    return(
        <div className="task">
            <input className="checkbox" type="checkbox" id={task.uid} checked={task.ready} onChange={handleToggleTask}/>
            <label for={task.uid}>{ task.title }</label>
        </div>
    )
}