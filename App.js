import React, { useState, useEffect } from 'react';
import DoToday from './DoToday';
import './css/master.css';

const DAILY_STATUS = "dotoday.tasks"
//const currentTime = new Date()

function App() {

  // Available tasks
  const [tasks, setTasks] = useState([{title: "Sleep", ready: false, uid: 1001},{title: "Breakfast", ready: false, uid: 1002},{title: "Lunch", ready: false, uid: 1003},{title: "Dinner", ready: false, uid: 1004},{title: "Vitamins", ready: false, uid: 1005},{title: "Train", ready: false, uid: 1006}])

  // Initial daily status load
  useEffect(() => {
    const storedStatus = JSON.parse(localStorage.getItem(DAILY_STATUS))
    if(storedStatus) setTasks(storedStatus)
  }, [])

  // Saves changes to local storage, initiated on any changes to tasks array.
  useEffect(() => {
    localStorage.setItem(DAILY_STATUS, JSON.stringify(tasks))
  }, [tasks])

  // Set tasks state to ready / not ready
  // Needs task id passed as value, then uses that id to find and edit copy of the task to be used in the setState function.
  function toggleTask(uid){
    const newTasks = [...tasks]
    const task = newTasks.find(task => task.uid === uid)
    task.ready = !task.ready
    setTasks(newTasks)
  }

  return (
    <div id="main">
      <div id="taskContainer">
        <DoToday toggleTask={toggleTask} tasks={tasks}/>
      </div>
      
      <h1>DYEL?</h1>
    </div>
    
  )
}

export default App;
