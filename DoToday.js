import React from 'react';
import Task from './Task';

export default function DoToday({ toggleTask, tasks }){
    return(
            tasks.map(task => <Task key={task.uid} toggleTask={toggleTask} task={task} />
        )
    )
}